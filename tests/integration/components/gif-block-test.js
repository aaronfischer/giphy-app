import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';

module('Integration | Component | gif-block', function(hooks) {
  setupRenderingTest(hooks);
  setupMirage(hooks);

  test('it renders', async function(assert) {
    this.set('selectedSize', 'fixed_width');
    this.set('item', server.create('gif', {
      images: {
        fixed_width: {
          url: 'http://google.com/test-image',
        },
      },
    }));

    await render(hbs`{{gif-block model=item imageSize=selectedSize}}`);

    assert.equal(this.element.getElementsByTagName('img')[0].getAttribute('src').trim(), 'http://google.com/test-image', 'Displays img src');
  });

  test('it renders other image sizes', async function(assert) {
    this.set('selectedSize', 'down_sized');
    this.set('item', server.create('gif', {
      images: {
        fixed_width: {
          url: 'http://google.com/fixed-width',
        },
        down_sized: {
          url: 'http://google.com/down-image',
        },
      },
    }));

    await render(hbs`{{gif-block model=item imageSize=selectedSize}}`);

    assert.equal(this.element.getElementsByTagName('img')[0].getAttribute('src').trim(), 'http://google.com/down-image', 'Displays different img src');
  });
});
