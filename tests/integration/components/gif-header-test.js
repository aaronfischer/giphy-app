import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, triggerEvent } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | gif-header', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    await render(hbs`{{gif-header}}`);

    assert.dom(this.element.getElementsByTagName('h1')[0]).hasText('Giphy Search', 'App title');
    assert.equal(this.element.querySelectorAll('#search').length, '1', 'Has search input');
  });

  test('it renders with search term', async function(assert) {
    this.set('term', 'Test');
    await render(hbs`{{gif-header search=term}}`);

    assert.equal(this.element.querySelectorAll('#search').length, '1', 'Has search input');
    assert.equal(this.element.querySelectorAll('#search')[0].value, 'Test', 'Search value filled in');
    assert.equal(this.element.querySelectorAll('.image-size').length, 0, 'Image size filter does not appear without data');
  });

  test('it renders displayedURL', async function(assert) {
    this.set('displayedURL', 'http://google.com/test-image');
    await render(hbs`{{gif-header displayedURL=displayedURL}}`);

    assert.equal(this.element.querySelectorAll('.displayed-url').length, 1, 'Displayed url is visible');
    assert.equal(this.element.querySelectorAll('.displayed-url input')[0].value, 'http://google.com/test-image', 'Value of displayed URL');
  });

  test('it resets displayedURL on search focus', async function(assert) {
    this.set('displayedURL', 'http://google.com/test-image');
    // mock the controller action does
    this.set('focusSearch', () => {
      this.set('displayedURL', null);
    })
    await render(hbs`{{gif-header displayedURL=displayedURL focusSearch=(action focusSearch)}}`);

    assert.equal(this.element.querySelectorAll('.displayed-url').length, 1, 'Displayed url is visible');

    await triggerEvent(this.element.querySelectorAll('#search')[0], 'focus');

    assert.equal(this.element.querySelectorAll('.displayed-url').length, 0, 'Displayed url is hidden');
  });
});
