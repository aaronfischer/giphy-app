import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | click-to-copy', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    this.set('displayedURL', 'http://google.com/test-image');
    await render(hbs`{{click-to-copy model=displayedURL}}`);

    assert.equal(this.element.querySelectorAll('input')[0].value, 'http://google.com/test-image', 'Value of displayed URL');
  });
});
