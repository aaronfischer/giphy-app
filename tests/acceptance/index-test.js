import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';
import {
  visit,
  findAll,
  fillIn,
  click,
  currentURL
} from '@ember/test-helpers';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';

module('Acceptance | Index', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('Check for expected content', async function(assert) {
    server.createList('gif', 25);
    await visit('/');

    assert.equal(currentURL(), '/');
    assert.dom('#search').exists({ count: 1 });
    assert.dom('.show-trending').doesNotExist('Trending link hidden');
    assert.dom('.image-size').doesNotExist('Image size filter hidden');
    assert.dom('.displayed-url').doesNotExist('Displayed url is hidden');
    assert.dom('.gif-block img').exists({ count: 25 }, 'gifs rendered');

    let request = await server.pretender.handledRequests[0];
    let requestBody = await JSON.parse(request.responseText).data;
    assert.equal(request.url, 'https://api.giphy.com/v1/gifs/trending?api_key=DIkR68F2DaLbIHkrniSfSY51EylfAobr&limit=25', 'check trending url by default');
    assert.equal(requestBody.length, '25', 'check for 25 items');
  });

  test('Check search for gif', async function(assert) {
    server.createList('gif', 25);
    await visit('/');

    assert.equal(currentURL(), '/');

    await fillIn('#search', 'Test 123');

    assert.equal(currentURL(), '/?search=Test%20123', 'Supports query-params for search');
    assert.equal(findAll('#search')[0].value, 'Test 123', 'Search value filled in');
    assert.dom('.show-trending').exists({ count: 1 }, 'Trending link appeared with filled in search value');
    assert.dom('.image-size').exists({ count: 1 }, 'Image size filter appeared with filled in search value');
    assert.dom(findAll('.image-size')[0]).hasText('Fixed Width', 'Fixed Width selected by default');
    assert.dom('.gif-block img').exists({ count: 25 }, 'gifs rendered');

    // get second request
    let request = await server.pretender.handledRequests[1];
    let requestBody = await JSON.parse(request.responseText).data;
    assert.equal(request.url, 'https://api.giphy.com/v1/gifs/search?api_key=DIkR68F2DaLbIHkrniSfSY51EylfAobr&limit=25&q=Test%20123', 'check search url for param');
    assert.equal(requestBody.length, '25', 'check for 25 items');
  });

  test('Check search does not occur for less than 3 characters', async function(assert) {
    server.createList('gif', 25);
    await visit('/');

    assert.equal(currentURL(), '/');

    await fillIn('#search', 'Te');

    assert.equal(findAll('#search')[0].value, 'Te', 'Search value filled in');

    // get second request
    let requests = await server.pretender.handledRequests;
    assert.equal(requests.length, 1, 'Only 1 request, search request was never fired');
  });

  test('Check reset of search clicking trending link', async function(assert) {
    server.createList('gif', 25);
    await visit('/');

    assert.equal(currentURL(), '/');

    await fillIn('#search', 'Test 123');

    assert.equal(currentURL(), '/?search=Test%20123', 'Supports query-params for search');
    assert.equal(findAll('#search')[0].value, 'Test 123', 'Search value filled in');
    assert.dom('.show-trending').exists({ count: 1 }, 'Trending link appeared with filled in search value');

    await click('.show-trending a');

    // get third request (trending)
    let request = await server.pretender.handledRequests[2];
    let requestBody = await JSON.parse(request.responseText).data;
    assert.equal(request.url, 'https://api.giphy.com/v1/gifs/trending?api_key=DIkR68F2DaLbIHkrniSfSY51EylfAobr&limit=25', 'gifs were reset to trending');
    assert.equal(requestBody.length, '25', 'check for 25 items');
  });

  test('Check clicking gif will display its url', async function(assert) {
    server.createList('gif', 25, {
      url: 'http://google.com/test-url',
    });
    await visit('/');

    assert.equal(currentURL(), '/');

    await click(findAll('.gif-block')[0]);

    assert.dom('.displayed-url').exists({ count: 1 }, 'Displayed url is visible');
    assert.equal(findAll('.displayed-url input')[0].value, 'http://google.com/test-url', 'Value of displayed URL');
  });

  test('Check for failure fetching gifs', async function(assert) {
    server.get('/trending',
      {
        errors: [
          {
            status: '400',
          }
        ]
      },
      400
    );
    await visit('/');

    assert.equal(currentURL(), '/');
    assert.dom('.error').exists({ count: 1 }, 'Error is displayed');
    assert.dom(findAll('.error')[0]).hasText(
      'Whoa! It looks like something went wrong, try again.',
      'Check for error copy'
    );
  });

});
