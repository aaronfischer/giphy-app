import { Serializer } from 'ember-cli-mirage';

export default Serializer.extend({
  // key should always be data to match giphy
  keyForModel() {
    return 'data';
  },
});