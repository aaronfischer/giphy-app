import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  title() { return faker.lorem.words(); },
  url() { return faker.image.image(); },
  images() {
    return {
      fixed_width: {
        url: faker.image.image(),
      },
      down_sized: {
        url: faker.image.image(),
      },
      fixed_width_small_still: {
        url: faker.image.image(),
      },
    };
  },
});
