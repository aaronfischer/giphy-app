export default function() {
  this.urlPrefix = 'http://api.giphy.com';
  this.namespace = '/v1/gifs/';

  this.get('/trending', (schema) => {
    return schema.gifs.all();
  });
  this.get('/search', (schema) => {
    return schema.gifs.all();
  });
}
