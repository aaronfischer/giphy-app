import { get } from '@ember/object';
import { helper } from '@ember/component/helper';

export default helper(([collection, attrName, attrValue]) =>
  collection.find(el => get(el, attrName) === attrValue)
);
