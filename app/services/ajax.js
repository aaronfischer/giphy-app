import AjaxService from 'ember-ajax/services/ajax';

export default AjaxService.extend({
  host: 'https://api.giphy.com',
  namespace: 'v1/gifs',
});
