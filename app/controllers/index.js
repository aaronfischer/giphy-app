import { action } from '@ember/object';
import Controller from '@ember/controller';

import Macy from 'macy';

export default class IndexController extends Controller {
  queryParams = ['search'];
  search = null;
  selectedSize = 'downsized';
  displayedURL = null;

  init() {
    super.init(...arguments);
    this.imageSizes = [
      {
        value: 'fixed_width',
        label: 'Fixed Width',
      },
      {
        value: 'downsized',
        label: 'Down Sized',
      },
      {
        value: 'fixed_width_small_still',
        label: 'Fixed Width Small Still',
      },
    ];
  }

  reset() {
    this.setProperties({
      search: null,
      displayedURL: null,
      selectedSize: 'fixed_width',
    });
  }

  @action
  triggerFlexGrid() {
    const flexGrid = Macy({
      // See below for all available options.
      container: '.grid',
      mobileFirst: true,
      columns: 2,
      breakAt: {
        800: {
          columns: 3
        },
        992: {
          columns: 4
        },
        1300: {
          columns: 5
        }
      }
    });
  }

  @action
  displayURL(url) {
    this.set('displayedURL', url);
    window.scrollTo(0, 0);
  }

  @action
  resetSearch() {
    this.reset();
  }

  @action
  focusSearch() {
    this.set('displayedURL', null);
  }
}
