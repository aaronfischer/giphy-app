import { action } from '@ember/object';
import { classNames } from '@ember-decorators/component';
import Component from '@ember/component';

@classNames('input-group')
export default class ClickToCopy extends Component {
  isCopied = false;

  didReceiveAttrs() {
    super.didReceiveAttrs(...arguments);
    this.set('isCopied', false);
  }

  @action
  copyURL() {
    const el = this.$().find('input');
    const selected =
      document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;
    el.select();
    document.execCommand('copy');
    this.set('isCopied', true);
    if (selected) {
      document.getSelection().removeAllRanges();
      document.getSelection().addRange(selected);
    }
  }
}
