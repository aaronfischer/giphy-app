import { tagName } from '@ember-decorators/component';
import { inject as service } from '@ember/service';
import Component from '@ember/component';
import config from 'giphy-app/config/environment';
import { task, timeout } from 'ember-concurrency';
import { isBlank } from '@ember/utils';

@tagName('')
export default class GifContainer extends Component {
  @service
  ajax;

  error = null;

  init() {
    super.init(...arguments);
    this.data = [];
  }

  didInsertElement() {
    super.didInsertElement(...arguments);
    // handles initial load/query
    let query = this.query;
    if (query) {
      this.giphyTask.perform(query);
    } else {
      this.giphyTask.perform();
    }
  }

  didUpdateAttrs() {
    super.didUpdateAttrs(...arguments);
    let query = this.query;
    if (!query) {
      this.giphyTask.perform();
    } else {
      this.searchGiphy.perform(query);
    }
  }

  @(task(function* (term) {
    yield timeout(500);
    if (isBlank(term) || term.length <= 2) { return this.set('data', []); }
    return yield this.giphyTask.perform(term);
  }).restartable())
  searchGiphy;

  @(task(function* (term) {
    let url = '/trending';
    let params = {
      data: {
        'api_key': config.giphyAPIkey,
        limit: 150,
      },
    };
    if (term) {
      params.data.q = term;
      url = '/search';
    }
    let results = yield this.ajax.request(url, params)
      .then(data => data.data)
      .catch(() => {
        this.set('error', true);
      });
    return this.set('data', results);
  }).restartable())
  giphyTask;
}
