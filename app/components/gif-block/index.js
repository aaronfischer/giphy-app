import { computed } from '@ember/object';
import { alias } from '@ember/object/computed';
import Component from '@glimmer/component';

export default class GifHeaderComponent extends Component {
  tagName = '';
  model = null;

  @computed('model', 'args.imageSize')
  get activeSize() {
    const imageSize = this.args.imageSize;
    return this.args.model.images[imageSize];
  }

  @alias('activeSize.url') src;
  // src: readOnly('activeSize.url'),

  @computed('activeSize.width')
  get widthStyle() {
    return `width:${this.activeSize.width}px`;
  }

  @computed('activeSize.height')
  get heightStyle() {
    return `height:${this.activeSize.height}px`;
  }
}
